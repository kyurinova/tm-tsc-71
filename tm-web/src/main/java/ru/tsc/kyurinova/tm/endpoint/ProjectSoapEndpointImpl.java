package ru.tsc.kyurinova.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kyurinova.tm.dto.soap.*;
import ru.tsc.kyurinova.tm.util.UserUtil;

@Endpoint
public class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://kyurinova.tsc.ru/tm/dto/soap";

    @Autowired
    private IProjectDTOService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) throws Exception {
        return new ProjectFindAllResponse(projectService.findAllByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectAddRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectAddResponse add(@RequestPayload final ProjectAddRequest request) throws Exception {
        return new ProjectAddResponse(projectService.addByUserId(UserUtil.getUserId(), request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) throws Exception {
        return new ProjectSaveResponse(projectService.updateByUserId(UserUtil.getUserId(), request.getProject()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) throws Exception {
        return new ProjectFindByIdResponse(projectService.findOneByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectExistsByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectExistsByIdResponse existsById(@RequestPayload final ProjectExistsByIdRequest request) throws Exception {
        return new ProjectExistsByIdResponse(projectService.existsByUserIdAndId(UserUtil.getUserId(), request.getId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectCountRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectCountResponse count(@RequestPayload final ProjectCountRequest request) throws Exception {
        return new ProjectCountResponse(projectService.countByUserId(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) throws Exception {
        projectService.removeByUserIdAndId(UserUtil.getUserId(), request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) throws Exception {
        projectService.removeByUserId(UserUtil.getUserId(), request.getProject());
        return new ProjectDeleteResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteAllRequest", namespace = NAMESPACE)
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ProjectDeleteAllResponse clear(@RequestPayload final ProjectDeleteAllRequest request) throws Exception {
        projectService.clearByUserId(UserUtil.getUserId());
        return new ProjectDeleteAllResponse();
    }

}
