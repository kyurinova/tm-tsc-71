package ru.tsc.kyurinova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

@Component
public class TaskIsBindToProjectByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-bind-to-project-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Bind task to project by id...";
    }

    @Override
    @EventListener(condition = "@taskIsBindToProjectByIdListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @Nullable final SessionDTO session = sessionService.getSession();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        if (projectEndpoint.findByIdProject(session, projectId) == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        if (taskEndpoint.findByIdTask(session, taskId) == null) throw new TaskNotFoundException();
        projectTaskEndpoint.bindTaskById(session, projectId, taskId);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
