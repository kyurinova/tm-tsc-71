package ru.tsc.kyurinova.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.endpoint.Status;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class TaskChangeStatusByNameListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String command() {
        return "task-change-status-by-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change status by name...";
    }

    @Override
    @EventListener(condition = "@taskChangeStatusByNameListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("Enter name");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        taskEndpoint.changeStatusByNameTask(sessionService.getSession(), name, status);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
