package ru.tsc.kyurinova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;

@Component
public class DataXmlSaveFasterXMLListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "data-xml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveFasterXMLListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA XML SAVE]");
        adminDataEndpoint.dataXmlSaveFasterXML(sessionService.getSession());
    }

}
