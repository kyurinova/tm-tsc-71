package ru.tsc.kyurinova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kyurinova.tm.api.service.dto.ISessionDTOService;
import ru.tsc.kyurinova.tm.api.service.dto.IUserDTOService;
import ru.tsc.kyurinova.tm.configuration.ServerConfiguration;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.marker.UnitCategory;

import java.sql.SQLException;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private IUserDTOService userService;

    @NotNull
    private IProjectDTOService projectService;

    @NotNull
    private ProjectDTO project;

    @NotNull
    private String projectId;

    @NotNull
    private String projectName = "testProject";

    @NotNull
    private String projectDescription = "Description of project";

    @NotNull
    private String userId;

    public ProjectServiceTest() throws Exception  {
    }

    @Before
    public void before() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        projectService = context.getBean(IProjectDTOService.class);
        userService = context.getBean(IUserDTOService.class);
        userId = userService.create("test", "test").getId();
        project = projectService.create(userId, projectName, projectDescription);
        projectId = project.getId();
    }

    @Test
    @Category(UnitCategory.class)
    public void createByNameTest() throws Exception {
        @NotNull final String newProjectName = "newTestProject";
        final ProjectDTO newProject;
        newProject = projectService.create(userId, newProjectName);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertNotNull(projectService.findByName(userId, newProjectName));
        projectService.remove(userId, newProject);
    }

    @Test
    @Category(UnitCategory.class)
    public void createTest() throws Exception {
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        final ProjectDTO newProject;
        newProject = projectService.create(userId, newProjectName, newProjectDescription);
        Assert.assertEquals(2, projectService.findAll().size());
        Assert.assertNotNull(projectService.findByName(userId, newProjectName));
        Assert.assertEquals(newProjectDescription, projectService.findByName(userId, newProjectName).getDescription());
        projectService.remove(userId, newProject);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByProjectTest() throws Exception {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertEquals(project.getId(), projectService.findById(userId, projectId).getId());
        Assert.assertEquals(project.getId(), projectService.findByIndex(userId, 0).getId());
        Assert.assertEquals(project.getId(), projectService.findByName(userId, projectName).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void existsProjectTest() throws Exception  {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertTrue(projectService.existsById(userId, projectId));
        Assert.assertTrue(projectService.existsByIndex(userId, 0));
    }

    @Test
    @Category(UnitCategory.class)
    public void updateByIdTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project.getId(), projectService.findByName(userId, projectName).getId());
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateById(userId, projectId, newProjectName, newProjectDescription);
        Assert.assertEquals(project.getId(), projectService.findByName(userId, newProjectName).getId());
        @NotNull final ProjectDTO newProject = projectService.findByName(userId, newProjectName);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void updateByIndexTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        Assert.assertNotNull(projectDescription);
        Assert.assertEquals(project.getId(), projectService.findById(userId, projectId).getId());
        @NotNull final String newProjectName = "newTestProject";
        @NotNull final String newProjectDescription = "newTestProjectDescription";
        projectService.updateByIndex(userId, 0, newProjectName, newProjectDescription);
        Assert.assertEquals(project.getId(), projectService.findByIndex(userId, 0).getId());
        @NotNull final ProjectDTO newProject = projectService.findByIndex(userId, 0);
        Assert.assertEquals(userId, newProject.getUserId());
        Assert.assertEquals(projectId, newProject.getId());
        Assert.assertEquals(newProjectName, newProject.getName());
        Assert.assertEquals(newProjectDescription, newProject.getDescription());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIdTest() throws Exception  {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.removeById(userId, projectId);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByIndexTest() throws Exception  {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        projectService.removeByIndex(userId, 0);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeProjectByNameTest() throws Exception  {
        Assert.assertNotNull(project);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.removeByName(userId, projectName);
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIdTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.startById(userId, projectId);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIndexTest() throws Exception  {
        Assert.assertNotNull(userId);
        projectService.startByIndex(userId, 0);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByNameTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.startByName(userId, projectName);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIdTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.finishById(userId, projectId);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIndexTest() throws Exception  {
        Assert.assertNotNull(userId);
        projectService.finishByIndex(userId, 0);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByNameTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.finishByName(userId, projectName);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIdTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        projectService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findById(userId, projectId).getStatus());
        projectService.changeStatusById(userId, projectId, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findById(userId, projectId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIndexTest() throws Exception  {
        Assert.assertNotNull(userId);
        projectService.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByIndex(userId, 0).getStatus());
        projectService.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByNameTest() throws Exception  {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectName);
        projectService.changeStatusByName(userId, projectName, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, projectService.findByName(userId, projectName).getStatus());
        projectService.changeStatusByName(userId, projectName, Status.NOT_STARTED);
        Assert.assertEquals(Status.NOT_STARTED, projectService.findByName(userId, projectName).getStatus());
    }

    @After
    public void after() throws SQLException {
        projectService.removeById(userId, projectId);
        userService.removeById(userId);
    }

}
