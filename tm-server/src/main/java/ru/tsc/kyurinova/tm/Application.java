package ru.tsc.kyurinova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.kyurinova.tm.component.Bootstrap;
import ru.tsc.kyurinova.tm.configuration.ServerConfiguration;

public class Application {

    public static void main(final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start(args);
    }

}
