package ru.tsc.kyurinova.tm.service.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.api.service.model.ISessionService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIdException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyIndexException;
import ru.tsc.kyurinova.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.model.Session;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.repository.model.SessionRepository;
import ru.tsc.kyurinova.tm.repository.model.UserRepository;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    public SessionRepository sessionRepository;

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @Autowired
    private IPropertyService propertyService;

    @Override
    @NotNull
    @SneakyThrows
    @Transactional
    public Session open(@NotNull final String login, @NotNull final String password) {
        @Nullable final User user = userRepository.findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUser(user);
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.save(session);
        return sign(session);
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @Nullable final User user = userRepository.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSecret();
        final int cycle = propertyService.getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) {
        if (session == null) return;
        sessionRepository.deleteById(session.getId());
    }

    @Override
    public boolean exists(@NotNull final String sessionId) {
        final Optional<Session> session1 = sessionRepository.findById(sessionId);
        if (session1 != null) return true;
        else return false;
    }

    @Override
    public void validate(@NotNull final Session session) {
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUser().getId() == null || session.getUser().getId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final Session sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!exists(session.getId())) throw new AccessDeniedException();
    }

    @SneakyThrows
    @Override
    public void validate(@NotNull Session session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUser().getId();
        @Nullable final User user = userRepository.findById(userId).orElse(null);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    @Transactional
    public void remove(@Nullable final Session entity) {
        if (entity == null) throw new EntityNotFoundException();
        sessionRepository.delete(entity);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    @Transactional
    public void clear() {
        sessionRepository.deleteAll();
    }

    @Override
    public Optional<Session> findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return sessionRepository.findById(id);
    }

    @NotNull
    @Override
    public Session findByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return sessionRepository.findAll().get(index);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        sessionRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        String sessionId = findByIndex(index).getId();
        sessionRepository.deleteById(sessionId);
    }

    @Override
    public int getSize() {
        return (int) sessionRepository.count();
    }

}
