package ru.tsc.kyurinova.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.log.OperationEvent;
import ru.tsc.kyurinova.tm.log.OperationType;

@NoArgsConstructor
@Component
public class EntityListener implements PostInsertEventListener, PostDeleteEventListener, PostUpdateEventListener {

    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    @Override
    public void onPostInsert(final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    @Override
    public void onPostUpdate(final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(final EntityPersister persister) {
        return false;
    }

    private void log(@NotNull final OperationType type, @NotNull final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(type, entity));
    }

}
